# Troxia CRM v14.0

- Troxia Enterprise Cloud Applications
- Funnel Marketing

Credits
=======

Contributors
------------
- Troxia Enterprise Cloud Applications

Sponsors
--------
- Troxia Enterprise Cloud Applications

Maintainers
-----------
- Troxia Enterprise Cloud Applications

Further information
===================
contact :- https://troxia.cloud