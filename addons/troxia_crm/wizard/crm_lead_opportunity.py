# -*- coding: utf-8 -*-
# Copyright 2020 Troxia.
# Part of Troxia. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models


class CrmLeadOpportunity(models.TransientModel):

    _inherit = ['crm.lead2opportunity.partner']


class CrmLeadMassOpportunity(models.TransientModel):

    _inherit = 'crm.lead2opportunity.partner.mass'
