/* Copyright 2020 Troxia.
    Part of Troxia. See LICENSE file for full copyright and licensing details. */
odoo.define('mm_crm_standalone.prioritywidget', function (require) {
    "use strict";
    const BasicFields = require('web.basic_fields');

    BasicFields.PriorityWidget.include({
        _renderStar: function (tag, isFull, index, tip) {
            return $(tag)
                .attr('title', tip)
                .attr('data-index', index)
                .addClass('o_priority_star fa')
                .toggleClass('fa-circle', isFull)
                .toggleClass('fa-circle-o', !isFull);
        },

        _onMouseOver: function (e) {
            clearTimeout(this.hoverTimer);
        },
    });


});