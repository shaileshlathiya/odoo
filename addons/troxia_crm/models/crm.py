# -*- coding: utf-8 -*-
# Copyright 2020 Troxia.
# Part of Troxia. See LICENSE file for full copyright and licensing details.
from odoo import fields, models, api, tools

AVAILABLE_PRIORITIES = [
    ('0', 'Normal'),
    ('1', 'Very Low'),
    ('2', 'Low'),
    ('3', 'Medium'),
    ('4', 'High'),
    ('5', 'Very High'),
]


class CrmLead(models.Model):

    _inherit = "crm.lead"

    priority = fields.Selection(selection_add=AVAILABLE_PRIORITIES, string='Priority', index=True, default=AVAILABLE_PRIORITIES[0][0])
    email = fields.Char(string="Email")
    # Contact and its related
    contact_id = fields.Many2one('res.partner', string='Existing Contact')
    c_phone = fields.Char('Contact Phone', related='contact_id.phone')
    c_mobile = fields.Char('Contact Mobile ', related='contact_id.mobile')
    c_email = fields.Char('Contact Email ', related='contact_id.email')

    # last name alias and external note for common
    firstname = fields.Char('Name')
    lastname = fields.Char('Surname')
    company = fields.Char("Company")
    alias_email = fields.Char('Alias Email')
    external_description = fields.Text('Description')
    main_phone = fields.Char("Source Phone")
    main_mobile = fields.Char("Source Mobile")
    lead_source = fields.Char("Lead Source")
    lead_source_dummy = fields.Char(
        related='lead_source', string='Lead Source')

    @api.onchange('source_id')
    def onchange_source_id(self):
        if self.source_id:
            self.lead_source = self.source_id.name


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.model
    def create(self, vals):
        if self.env.context and self.env.context.get('quotation_from_opportunity') and vals.get('partner_id', False):
            partner = self.env['res.partner'].browse([vals.get('partner_id')])
            if partner:
                partner.is_customer = True
                partner.is_contact = False

        return super(SaleOrder, self).create(vals)


class Lead2OpportunityPartner(models.TransientModel):
    _inherit = 'crm.lead2opportunity.partner'

    new_opportunity_topic = fields.Char("Opportunity Topic", )
    lead_topic = fields.Char("Lead Topic", store=True)

    @api.model
    def default_get(self, fields):
        """ Default get for name, opportunity_ids.
            If there is an exisitng partner link to the lead, find all existing
            opportunities links with this partner to merge all information together
        """
        result = super(Lead2OpportunityPartner, self).default_get(fields)
        if result:
            leads = self.env['crm.lead'].browse(int(result.get('lead_id')))
            if len(leads) == 1:
                result['lead_topic'] = leads.name
                result['new_opportunity_topic'] = leads.name
        return result

    def _convert_and_allocate(self, leads, user_ids, team_id=False):
        self.ensure_one()
        if len(leads) == 1:
            leads.name = self.new_opportunity_topic
        res = super(Lead2OpportunityPartner, self)._convert_and_allocate(leads, user_ids, team_id)
        return res


