# -*- coding: utf-8 -*-
# Copyright 2020 Troxia.
# Part of Troxia. See LICENSE file for full copyright and licensing details.
from odoo import fields, models, api, tools


class ResPartner(models.Model):

    _inherit = "res.partner"

    @api.model
    def default_get(self, fields):
        result = super(ResPartner, self).default_get(fields)
        if self._context.get('crm_view', False):
            if self._context.get('default_firstname'):
                result.update(
                    {'firstname': self._context.get('default_firstname')})
            if self._context.get('default_firstname'):
                result.update(
                    {'lastname': self._context.get('default_lastname')})
            result.update({'customer': self._context.get('default_customer')})
        return result
