# -*- coding: utf-8 -*-
# Copyright 2020 Troxia.
# Part of Troxia. See LICENSE file for full copyright and licensing details.
{
    "name": "Troxia - CRM",
    'category': 'Partners',
    'version': '14.0.0.1',
    'summary': 'Troxia CRM',
    'description': "Troxia CRM",
    'complexity': "medium",
    'depends': [
        'crm',
        'sale_crm',
        'sales_team',
        'partner_firstname',
        'utm',
        'link_tracker',
    ],
    'data': [
        # Security
        'security/ir.model.access.csv',
        'security/security.xml',
        # Wizard
        'wizard/crm_lead_opportunity.xml',
        # Views
        'views/assets.xml',
        'views/crm_lead.xml',
        'views/crm_pipeline.xml',
        'views/lead_management_menu.xml',
        'views/crm_menu.xml',
        'views/crm_config.xml'
    ],
    # Author
    'author': 'Troxia Enterprise Cloud Applications',
    'website': 'https://troxia.cloud',

    'installable': True,
    'auto_install': False,
    'application': False,
}
