$(document).ready(function() {
    $('.appointment_timezone').on('change', function() {
        $('.appointment_form').submit();
    });
    $('.jump_date').on('change', function() {
        $('.appointment_form').submit();
    });
});
