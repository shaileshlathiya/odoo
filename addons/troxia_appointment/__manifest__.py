# -*- coding: utf-8 -*-
{
    'name': 'Troxia Calendar Appointment',
    'version': '14.0.1.0',
    'summary': 'Troxia Calendar Appointment',
    'description': """
Troxia Calendar Appointment
===========================
    """,
    'author': 'Troxia.',
    'images': ['static/description/main_screenshot.jpeg'],
    'category': 'website',
    'depends': ['website', 'mail', 'calendar', 'contacts'],
    'data': [
        'data/appointment_data.xml',
        'views/res_partner.xml',
        'views/calendar_view.xml',
        'views/appointment_template.xml',
        'views/calendar_asset.xml',
        'security/ir.model.access.csv',
        # 'security/ir_rule.xml',
    ],
    'installable': False,
    'application': False,
    'auto_install': False,

}
