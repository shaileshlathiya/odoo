# -*- coding: utf-8 -*-
# Copyright 2020 Troxia.
# Part of Troxia. See LICENSE file for full copyright and licensing details.
from odoo import models, fields, api, _


class ResUser(models.Model):
    _inherit = 'res.users'

    related_partner_company = fields.Many2one('res.partner', 'Company Related ')

    @api.model
    def create(self, vals):
        res = super(ResUser, self).create(vals)

        if vals.get('related_partner_company', False) and res.partner_id:
            res.partner_id.parent_id = vals.get('related_partner_company')
            res.partner_id.is_team_member = True
            res.partner_id.customer_rank = 0
        return res

    def write(self, vals):
        result = super(ResUser, self).write(vals)
        for user in self:
            if vals.get('related_partner_company', False) and user.partner_id:
                user.partner_id.parent_id = vals.get('related_partner_company')
                user.partner_id.is_team_member = True
                user.partner_id.customer_rank = 0
        return result
