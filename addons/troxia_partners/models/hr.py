# -*- coding: utf-8 -*-
# Copyright 2020 Troxia.
# Part of Troxia. See LICENSE file for full copyright and licensing details.
from odoo import fields, models, api


class Employee(models.Model):

    _inherit = 'hr.employee'

    @api.model
    def create(self, vals):
        res = super(Employee, self).create(vals)
        if res and vals.get('address_home_id'):
            res.address_home_id.employee_id = res.id
            res.address_home_id.is_employee = True
        return res

    def write(self, vals):
        res = super(Employee, self).write(vals)
        for employee in self:
            if res and vals.get('address_home_id'):
                employee_partners = self.env['res.partner'].search(
                    [('employee_id', '=', employee.id)])
                for partner in employee_partners:
                    partner.employee_id = False
                    partner.is_employee = False

                if employee.address_home_id:
                    employee.address_home_id.write(
                        {'employee_id': employee.id, 'is_employee': True})
        return res
