# -*- coding: utf-8 -*-
# Copyright 2020 Troxia.
# Part of Troxia. See LICENSE file for full copyright and licensing details.
from odoo import fields, models, api, _
from odoo.exceptions import ValidationError


class BusinessCriticals(models.Model):
    _name = 'business.critical'
    _rec_name = 'fields_critical'

    fields_critical = fields.Selection(
        [('vat', 'VAT'), ('fiscal', 'Fiscal')], "Field")
    is_require = fields.Boolean("Is Mandatory?")

    @api.model
    def create(self, vals):
        for record in self.env['business.critical'].search([]):
            if vals.get('fields_critical') == record.fields_critical:
                raise ValidationError(
                    _('Field is Already Used, Please set Existing!!'))

        if vals.get('fields_critical') and vals.get('fields_critical') == 'vat':
            set_param = self.env['ir.config_parameter'].set_param
            set_param('troxia_partners.vat_require', '1')
        if vals.get('is_require') is False and vals.get('fields_critical') == 'vat':
            set_param = self.env['ir.config_parameter'].set_param
            set_param('troxia_partners.vat_require', '0')

        if vals.get('fields_critical') and vals.get('fields_critical') == 'fiscal':
            set_param = self.env['ir.config_parameter'].set_param
            set_param('troxia_partners.fiscal_require', '1')
        if vals.get('is_require') is False and vals.get('fields_critical') == 'fiscal':
            set_param = self.env['ir.config_parameter'].set_param
            set_param('troxia_partners.fiscal_require', '0')
        return super(BusinessCriticals, self).create(vals)

    def write(self, vals):
        if vals.get('is_require') and self.fields_critical == 'vat':
            set_param = self.env['ir.config_parameter'].set_param
            set_param('troxia_partners.vat_require', '1')
        if vals.get('is_require') is False and self.fields_critical == 'vat':
            set_param = self.env['ir.config_parameter'].set_param
            set_param('troxia_partners.vat_require', '0')
        if vals.get('is_require') and self.fields_critical == 'fiscal':
            set_param = self.env['ir.config_parameter'].set_param
            set_param('troxia_partners.fiscal_require', '1')
        if vals.get('is_require') is False and self.fields_critical == 'fiscal':
            set_param = self.env['ir.config_parameter'].set_param
            set_param('troxia_partners.fiscal_require', '0')
        return super(BusinessCriticals, self).write(vals)


class BusinessCritical(models.TransientModel):
    _name = 'business.critial.settings'
    _inherit = 'res.config.settings'

    vat_require = fields.Boolean("Vat Mandatory?")
    fiscal_require = fields.Boolean("Fiscal Mandatory?")

    @api.model
    def get_values(self):
        res = super(BusinessCritical, self).get_values()
        vat_require = False
        fiscal_require = False
        get_param = self.env['ir.config_parameter'].sudo().get_param

        if get_param('troxia_partners.vat_require') == '1':
            vat_require = True
        if get_param('troxia_partners.fiscal_require') == '1':
            fiscal_require = True
        res.update(
            vat_require=vat_require,
            fiscal_require=fiscal_require,
        )
        return res

    def set_values(self):
        super(BusinessCritical, self).set_values()
        set_param = self.env['ir.config_parameter'].set_param
        set_param('troxia_partners.vat_require',
                  self.vat_require and '1' or '0')
        set_param('troxia_partners.fiscal_require',
                  self.fiscal_require and '1' or '0')
