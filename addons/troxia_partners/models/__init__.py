# -*- coding: utf-8 -*-
# Copyright 2020 Troxia.
# Part of Troxia. See LICENSE file for full copyright and licensing details.
from . import res_partner
from . import hr
from . import res_users
from . import ir_config_parameter
from . import business_critical_config
