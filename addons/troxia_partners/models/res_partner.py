# -*- coding: utf-8 -*-
# Copyright 2020 Troxia.
# Part of Troxia. See LICENSE file for full copyright and licensing details.
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class Partner(models.Model):
    _inherit = "res.partner"

    _sql_constraints = [
        ('check_name', 'Check(1=1)', 'Contacts require a name.'),
    ]

    @api.depends('team_member_ids')
    def _get_related_contacts(self):
        for rec in self:
            for data in rec.team_member_ids:
                rec.related_contacts = [(4, data.id)]

    @api.depends('is_customer')
    def _get_customer_rank(self):
        for rec in self:
            if rec.is_customer:
                rec.customer_rank = 1
            else:
                rec.customer_rank = 0

    @api.depends('is_supplier')
    def _get_supplier_rank(self):
        for rec in self:
            if rec.is_supplier:
                rec.is_supplier = 1
            else:
                rec.is_supplier = 0

    def _get_associated_companies(self):
        for rec in self:
            id_list = self.search([("related_parent_company", "=", rec.id),
                                   ('related_company_type', '!=', 'legal_office')]).ids
            if rec.related_company_type != 'legal_office':
                id_list.append(rec.related_parent_company.id)
            rec.associated_companies = self.browse(id_list)

    is_customer = fields.Boolean('Customer')
    is_supplier = fields.Boolean('Supplier')
    is_contact = fields.Boolean('Contacts')
    is_employee = fields.Boolean('Employee')
    is_team_member = fields.Boolean('Team Member')
    is_prospect = fields.Boolean('Prospect')

    customer_rank = fields.Integer(store=True, compute=_get_customer_rank)
    supplier_rank = fields.Integer(store=True, compute=_get_supplier_rank)

    # Employee fields
    email2 = fields.Char("Email 2", copy=False)
    employee_id = fields.Many2one('hr.employee', string='Employee', copy=False)
    emp_name = fields.Char(related='employee_id.name', string='Name')
    address_id = fields.Many2one(
        'res.partner', related='employee_id.address_id', string="Working Address")
    mobile_phone = fields.Char(
        related='employee_id.mobile_phone', string='Work Phone')
    work_location = fields.Char(
        related='employee_id.work_location', string='Work Location')
    work_email = fields.Char(
        related='employee_id.work_email', string='Work Email')
    work_phone = fields.Char(
        related='employee_id.work_phone', string='Work Phone')
    department_id = fields.Many2one(
        'hr.department', related="employee_id.department_id", string="Department")
    coach_id = fields.Many2one(
        'hr.employee', related='employee_id.coach_id', string='Coach')
    job_id = fields.Many2one(
        'hr.job', related='employee_id.job_id', string='Job Title')
    emp_parent_id = fields.Many2one(
        'hr.employee', related='employee_id.parent_id', string='Manager')
    emp_country_id = fields.Many2one(
        'res.country', related='employee_id.country_id', string='Nationality (Country)')
    identification_id = fields.Char(
        related='employee_id.identification_id', string='Identification No')
    passport_id = fields.Char(
        related='employee_id.passport_id', string='Passport No')
    address_home_id = fields.Many2one(
        'res.partner', related='employee_id.address_home_id', string='Home Address')
    gender = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female'),
        ('other', 'Other')
    ], related='employee_id.gender', string="Gender")
    marital = fields.Selection([
        ('single', 'Single'),
        ('married', 'Married'),
        ('widower', 'Widower'),
        ('divorced', 'Divorced')
    ], related='employee_id.marital', string='Marital Status')
    birthday = fields.Date(related='employee_id.birthday', string='Birthday')
    # calendar_id = fields.Many2one('resource.calendar', related='employee_id.calendar_id', string="Working Time")
    team_member_ids = fields.Many2many(
        'res.partner', 'team_members_rel', 'rel_partner_id', 'team_member_id', string='Team Members')
    team_member_ids2 = fields.Many2many('res.partner', 'team_members_rel2', 'rel_partner_id', 'team_member_id',
                                        string='StakeHolders')

    related_contacts = fields.Many2many("res.partner", "related_contacts_partner_rel",
                                        'related_contact_id', 'partner_id', compute=_get_related_contacts, store=True)

    team_member_ids3 = fields.Many2many('res.partner', 'team_members_rel3', 'rel_partner_id', 'team_member_id',
                                        string='Related Contacts',)
    related_contacts_dummy = fields.Char("Related Contacts ")

    related_company_type = fields.Selection([('legal_office', 'Legal Office'),
                                             ('secondary_office',
                                              'Secondary Office'),
                                             ('local_unit', 'Local Unit')], string="Related Company Type",
                                            default='legal_office')
    related_parent_company = fields.Many2one(
        "res.partner", "Related Parent Company")
    associated_companies = fields.Many2many(
        "res.partner", string="Associated Companies", compute="_get_associated_companies")
    related_companies_tab = fields.Many2many(
        "res.partner", "related_companies_rel", "related_company_id", "res_partner_id", string="Related Companies")

    @api.onchange('company_type', 'is_team_member')
    def check_company_type(self):
        if self.is_team_member and self.company_type == 'company':
            warning = {
                'title': "User Error",
                'message': """Un azienda non puo essere un team member, perfavore cambia da Azienda a Persona oppure
                            disattiva il checkbox su team member!""",
            }
            self.update({'company_type': 'person', 'is_team_member': True})
            return {'warning': warning}

    @api.onchange('company_type', 'is_employee')
    def check_company_type_for_employee(self):

        if self.is_employee and self.company_type == 'company':
            warning = {
                'title': "User Error",
                'message': """Un azienda non puo essere un dipendente, perfavore cambia da Azienda a Persona oppure
                            disattiva il checkbox su dipendente!""",
            }
            self.update({'company_type': 'person', 'is_employee': True})
            return {'warning': warning}

    def name_get(self):
        res = []
        for partner in self:
            name = partner.name or ''
            res.append((partner.id, name))
        return res

    @api.onchange('team_member_ids')
    def set_stackholders(self):
        if self._context.get('team_members', False):
            self.team_member_ids2 = self.team_member_ids.ids
            self.team_member_ids3 = self.team_member_ids.ids

    @api.onchange('team_member_ids2')
    def set_team_members(self):
        if self._context.get('stack_holders', False):
            self.team_member_ids = self.team_member_ids2.ids
            self.team_member_ids3 = self.team_member_ids2.ids

    @api.onchange('team_member_ids3')
    def set_team_members_three(self):
        if self._context.get('related_contacts', False):
            self.team_member_ids = self.team_member_ids3.ids
            self.team_member_ids2 = self.team_member_ids3.ids

    def get_employee_fields(self):
        emp_fields = ['work_location', 'work_email', 'mobile_phone', 'department_id', 'coach_id', 'job_id', 'emp_parent_id',
                  'emp_country_id', 'identification_id', 'passport_id', 'address_home_id', 'gender', 'marital',
                  'address_id', 'work_phone', 'birthday', 'twitter_acc', 'linkedin_acc'
                  ]
        return emp_fields

    def get_employee_data(self, emp_fields, vals):
        data_dict = {}
        for field in emp_fields:
            if field in vals:
                if field == 'emp_parent_id':
                    data_dict.update({'parent_id': vals.get(field)})
                elif field == 'emp_country_id':
                    data_dict.update({'country_id': vals.get(field)})
                else:
                    data_dict.update({field: vals.get(field)})
        return data_dict

    def team_members(self, vals):
        if vals.get('team_member_ids') and vals.get('team_member_ids')[0] and len(
                vals.get('team_member_ids')[0]) >= 1:
            if vals.get('team_member_ids')[0][0] == 6:
                vals.update({'team_member_ids2': vals.get(
                    'team_member_ids'), 'team_member_ids3': vals.get('team_member_ids')})
        if vals.get('team_member_ids2') and vals.get('team_member_ids2')[0] and len(
                vals.get('team_member_ids2')[0]) >= 1:
            if vals.get('team_member_ids2')[0][0] == 6:
                vals.update({'team_member_ids': vals.get(
                    'team_member_ids2'), 'team_member_ids3': vals.get('team_member_ids2')})
        if vals.get('team_member_ids3') and vals.get('team_member_ids3')[0] and len(
                vals.get('team_member_ids3')[0]) >= 1:
            if vals.get('team_member_ids3')[0][0] == 6:
                vals.update({'team_member_ids': vals.get(
                    'team_member_ids3'), 'team_member_ids2': vals.get('team_member_ids3')})

    def write(self, vals):
        if vals.get('is_contact'):
            vals.update(
                {'customer_rank': 0, 'supplier_rank': 0, 'is_supplier': False, 'is_customer': False, 'is_employee': False})
        elif vals.get('is_customer') or vals.get('is_supplier') or vals.get('is_employee'):
            vals.update({'is_contact': False})

        for partner in self:
            if not self._context.get('from_team_member', False):
                self.team_members(vals)
                if vals.get('is_employee') and not partner.employee_id:
                    vals.update({'emp_name': self.name})
                    emp_fields = partner.get_employee_fields()
                    emp_value = partner.get_employee_data(emp_fields, vals)
                    emp_value.update(
                        {'name': partner.name, 'address_home_id': partner.id})
                    emp = self.env['hr.employee'].create(emp_value)
                    if emp:
                        vals.update({'employee_id': emp.id})

                if vals.get('parent_id') or vals.get('is_team_member'):
                    if partner.company_type == 'person' or vals.get(
                            'company_type') == 'person' and partner.is_team_member or \
                            vals.get('is_team_member') and vals.get('parent_id') or partner.parent_id:
                        if vals.get('parent_id'):
                            if partner.parent_id:
                                partner.parent_id.team_member_ids = [
                                    (3, partner.id)]
                            existing_partner = self.env['res.partner'].browse(
                                vals.get('parent_id'))
                            existing_partner.team_member_ids = [
                                (4, partner.id)]
                        else:
                            partner.parent_id.team_member_ids = [
                                (4, partner.id)]
                if vals.get('team_member_ids') and partner.company_type == 'company':
                    new_list = vals.get('team_member_ids')[0]
                    old_list = partner.team_member_ids.ids
                    if len(new_list) > len(old_list):
                        filtered_data = list(set(new_list) - set(old_list))
                        if filtered_data:
                            for internal_partner in self.env['res.partner'].browse(filtered_data):
                                internal_partner.with_context(
                                    from_team_member=True).parent_id = partner.id

        res = super(Partner, self).write(vals)
        return res

    @api.model
    def create(self, vals):
        if vals.get('is_contact'):
            vals.update(
                {'is_customer': False, 'is_supplier': False, 'is_employee': False, 'is_prospect': False})
        elif vals.get('is_customer') or vals.get('is_supplier') or vals.get('is_employee') or vals.get('is_prospect'):
            vals.update({'is_contact': False})

        self.team_members(vals)
        res = super(Partner, self).create(vals)

        if vals.get('parent_id') and vals.get('company_type') == 'person' and vals.get('is_team_member'):
            parent_id = vals.get('parent_id')
            company = self.env['res.partner'].browse(parent_id)
            if company:
                if res.id not in company.team_member_ids.ids:
                    company.team_member_ids = [(4, res.id)]

        if res.company_type == 'person':
            res.color = 1
        else:
            res.color = 6

        if res.is_employee and not res.employee_id:
            emp_fields = self.get_employee_fields()
            emp_value = self.get_employee_data(emp_fields, vals)
            emp_value.update({'name': res.name, 'address_home_id': res.id})
            employee = self.env['hr.employee'].create(emp_value)

            if employee:
                res.employee_id = employee.id
        return res

    @api.onchange('name')
    def onchange_name(self):
        if self.employee_id:
            self.emp_name = self.name
        elif self.is_employee:
            emp = self.self.env['hr.employee'].search([('firstname', '=', self.firstname), (
                'lastname', '=', self.lastname), ('active', '=', False), ('identification_id', '=', self.fiscalcode)])
            if emp:
                self.employee_id = emp[0].id
                emp[0].address_home_id = self._origin.id
                emp.active = True

    @api.onchange('is_employee', 'is_contact', 'is_customer', 'is_supplier')
    def set_check_flag_status(self):
        if self.is_employee or self.is_customer or self.is_supplier or self.is_team_member or self.is_prospect:
            self.is_contact = False
        if self.is_contact:
            self.is_employee = False
            self.is_customer = False
            self.customer_rank = 0
            self.supplier_rank = 0
            self.is_team_member = False
            self.is_prospect = False

    @api.onchange('is_employee')
    def onchange_is_employee(self):
        if not self.employee_id and self.is_employee and self._origin:
            emp = self.env['hr.employee'].search(
                [('address_home_id', '=', self._origin.id)])
            if emp:
                self.employee_id = emp[0].id

            elif self.lastname and self.firstname:
                emp = self.self.env['hr.employee'].search([('firstname', '=', self.firstname), (
                    'lastname', '=', self.lastname), ('active', '=', False),
                                                           ('identification_id', '=', self.fiscalcode)])
                if emp:
                    self.employee_id = emp[0].id
                    emp[0].address_home_id = self._origin.id
                    emp[0].active = True

        elif self.employee_id and not self.is_employee:
            self.employee_id.active = False

    @api.model
    def search(self, args, offset=0, limit=None, order=None, count=False):

        if not self._context.get("internal_call_of_search"):
            to_be_search = [
                data for data in args if 'related_contacts_dummy' in data]
            if to_be_search:
                related_search = []
                for partner in self.env['res.partner'].with_context(internal_call_of_search=True).search([]):
                    for related_contact in partner.related_contacts:
                        if to_be_search[0][2] and to_be_search[0][2].lower() in related_contact.name.lower():
                            related_search.append(partner.id)
                args.remove(to_be_search[0])
                args.append(('id', 'in', related_search))

        if self._context.get('team_members', False) or self._context.get('stack_holders', False):
            if args is None:
                args = []
            customer = self.env.context.get('default_customer_rank', 0)
            supplier = self.env.context.get('default_supplier_rank', 0)
            contact = self.env.context.get('team_stake_contact', False)
            employee = self.env.context.get('team_stake_employee', False)
            team_member = self.env.context.get('default_is_team_member', False)

            args1 = []
            new_args1 = []
            if customer == 1:
                args1.append(('is_customer', '=', True))
            if supplier == 1:
                args1.append(('is_supplier', '=', True))
            if contact:
                args1.append(('is_contact', '=', True))
            if employee:
                args1.append(('is_employee', '=', True))
            if team_member:
                args1.append(('is_team_member', '=', True))

            if len(args1) >= 2:
                for arg_domain in args1[0:-1]:
                    new_args1.append('|')
                    new_args1.append(arg_domain)
                new_args1.append(args1[-1])
            else:
                new_args1 = args1
            args += new_args1

        if self._context.get("related_companies_tab", False):

            active_id = self._context.get("related_companies_tab")
            rec = self.browse(active_id)

            if args is None:
                args = []
            args_temp = []
            new_args = [('is_company', '=', True)]

            if rec.is_customer:
                args_temp.append(('is_customer', '=', True))
            if rec.is_supplier:
                args_temp.append(('is_supplier', '=', True))
            if rec.is_contact:
                args_temp.append(('is_contact', '=', True))
            if rec.is_employee:
                args_temp.append(('is_employee', '=', True))
            if rec.is_team_member:
                args_temp.append(('is_team_member', '=', True))

            if len(args_temp) >= 2:
                for arg_domain in args_temp[0:-1]:
                    new_args.append('|')
                    new_args.append(arg_domain)
                new_args.append(args_temp[-1])
            else:
                new_args += args_temp
            args += new_args

        return super(Partner, self).search(args, offset=offset, limit=limit, order=order, count=count)

    @api.model
    def default_get(self, default_fields):
        res = super(Partner, self).default_get(default_fields)
        if self._context.get("related_companies_tab", False):
            res.update({'is_company': True})
        return res

    def call_warning_for_fiscalcode(self):
        raise UserError(
            _('E mandatorio inputare il codice fiscale nel tab Contabilita!'))

    def call_warning_for_vatcode(self):
        raise UserError(
            _('E mandatorio inputare il  Partita IVA nel tab Contabilita!'))
