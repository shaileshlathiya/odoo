# -*- coding: utf-8 -*-
# Copyright 2020 Troxia.
# Part of Troxia. See LICENSE file for full copyright and licensing details.
{
    "name": "Troxia Partners",
    'category': 'Partners',
    'version': '14.0.0.1',
    'summary': 'Troxia Partners',
    'description': "Troxia Partners",
    'depends': [
        'contacts',
        'account',
        'hr_employee_firstname',
        'troxia_partner_roles_permissions',
        'l10n_it_fiscalcode',
        'l10n_it_pec',
        'troxia_partner_fields',
        'portal',
        'base_geolocalize',
        'partner_contact_personal_information_page',
        'troxia_partner_ident-passport-ID'],
    'data': [
        #data
        'data/ir_config_parameter.xml',
        #security
        'security/ir.model.access.csv',
        #wizard
        'wizard/search_partners_view.xml',
        #views
        'views/troxia_partner_asset.xml',
        'views/res_partner_view_inherit.xml',
        'views/res_users_view.xml',
        'views/business_critical_view.xml',
    ],
    # Author
    'author': 'Troxia Enterprise Cloud Applications',
    'website': 'https://troxia.cloud',

    'installable': True,
    'application': True,
    'auto_install': False,
}
