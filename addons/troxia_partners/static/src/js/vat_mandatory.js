/* Copyright 2020 Troxia.
    Part of Troxia. See LICENSE file for full copyright and licensing details. */
odoo.define('troxia_partners.VatAlertMandatory', function (require) {
    "use strict";

    const core = require('web.core');
    const rpc = require('web.rpc');
    const FormController = require('web.FormController');
    const _t = core._t;

    FormController.include({

        _onSave: function (ev) {
            const self = this;
            if (self.modelName === 'res.partner') {
                var data = self.renderer.state.data
                if (data['vat'] === false && data['related_company_type'] !== 'local_unit' && data['company_type'] === 'company' && (data['customer'] === true || data['supplier'] === true)) {
                    self.do_notify(_t(''), _t("E mandatorio inputare il  Partita IVA nel tab Contabilita!"), true);
                    return;
                }
            }
            return this._super.apply(this, arguments);
        },
    });

});