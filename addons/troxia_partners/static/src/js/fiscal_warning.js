/* Copyright 2020 Troxia.
    Part of Troxia. See LICENSE file for full copyright and licensing details. */
odoo.define('troxia_partners.FormView', function (require) {
    "use strict";


    const core = require('web.core');
    const FormController = require('web.FormController')

    FormController.include({
        _onSave: function (ev) {
            const self = this
            if (self.modelName === 'res.partner') {
                const data = self.renderer.state.data
                if (data['fiscalcode'] === false && data['company_type'] === 'person' && data['parent_id'] === false && (data['customer'] === true || data['supplier'] === true)) {
                    if (confirm("Non sono stai inseriti i seguenti campi business critical: \n - Codice Fiscale \n Vuoi consentire in ogni caso il salvataggio?") === true) {
                        return this._super.apply(this, arguments);
                    } else {
                        return;
                    }
                }
            }
            return this._super.apply(this, arguments);
        },

    });

});