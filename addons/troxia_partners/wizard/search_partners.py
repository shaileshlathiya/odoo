# -*- coding: utf-8 -*-
# Copyright 2020 Troxia.
# Part of Troxia. See LICENSE file for full copyright and licensing details.
from odoo import fields, models, api, _


class SearchPartners(models.TransientModel):
    _name = 'search.partners'
    _description = "Partner"

    name = fields.Char("Search Text")
    customer = fields.Boolean("Customer")
    contacts = fields.Boolean("Contacts")
    supplier = fields.Boolean("Supplier")
    employee = fields.Boolean("Employee")
    team_member = fields.Boolean("Team Member")
    select_all = fields.Boolean("Select All")

    @api.onchange('select_all')
    def onchange_select_all(self):
        if self.select_all:
            self.customer = True
            self.contacts = True
            self.supplier = True
            self.employee = True
            self.team_member = True
        else:
            self.customer = False
            self.contacts = False
            self.supplier = False
            self.employee = False
            self.team_member = False

    def default_search_partner_domain(self, default_dict):
        if not self.select_all:
            if self.customer:
                default_dict.update({'default_customer_rank': 1})
            if self.supplier and not self.customer:
                default_dict.update(
                    {'default_customer_rank': 0, 'default_supplier_rank': 1})
            if self.contacts and not self.customer:
                default_dict.update(
                    {'default_customer_rank': 0, 'default_is_contact': True})
            if self.employee and not self.customer:
                default_dict.update(
                    {'default_customer_rank': 0, 'default_is_employee': True})
            if self.team_member and not self.customer:
                default_dict.update(
                    {'default_customer_rank': 0, 'default_is_team_member': True})
            if self.supplier and self.customer:
                default_dict.update({'default_supplier_rank': 1})
            if self.contacts and self.customer:
                default_dict.update({'default_is_contact': True})
            if self.employee and self.customer:
                default_dict.update({'default_is_employee': True})
            if self.team_member and self.customer:
                default_dict.update({'default_is_team_member': True})
        if self.select_all:
            default_dict.update({'default_customer_rank': 0})

    def create_partner_info(self):
        self.ensure_one()
        default_dict = {}
        if not self.name:
            self.default_search_partner_domain(default_dict)

        self.default_search_partner_domain(default_dict)

        if not default_dict:
            default_dict.update({'default_customer_rank':  0})
        view_id = self.env.ref('base.view_partner_form').id
        default_dict.update({'default_name':  self.name})
        return {
            'name': _('Partners'),
            'view_mode': 'tree',
            'views': [(view_id, 'form')],
            'res_model': 'res.partner',
            'view_id': view_id,
            'type': 'ir.actions.act_window',
            'context':   default_dict,
        }

    def partner_view_return(self, partner_ids):
        tree_view = self.env.ref('base.view_partner_tree')
        form_view = self.env.ref('base.view_partner_form')
        return {
            'name': _('Partners'),
            'view_mode': 'tree,form',
            'res_model': 'res.partner',
            'views': [
                (tree_view.id, 'tree'),
                (form_view.id, 'form')
            ],
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', partner_ids)]
        }

    def partner_domain(self, domain):
        if self.customer:
            domain.append(('is_customer', '=', True))
        if self.supplier:
            domain.append(('is_supplier', '=', True))
        if self.contacts:
            domain.append(('is_contact', '=', True))
        if self.employee:
            domain.append(('is_employee', '=', True))
        if self.team_member:
            domain.append(('is_team_member', '=', True))

    def search_partners_info(self):
        """
        This method will help to return the partners associated data according their status.
        :return action of view with searched records.

        """
        self.ensure_one()
        domain = []
        if not self.name:
            self.partner_domain(domain)
            if self.select_all:
                tree_view = self.env.ref('base.view_partner_tree')
                form_view = self.env.ref('base.view_partner_form')
                return {
                    'name': _('Partners'),
                    'view_mode': 'tree,form',
                    'res_model': 'res.partner',
                    'views': [
                        (tree_view.id, 'tree'),
                        (form_view.id, 'form')
                    ],
                    'type': 'ir.actions.act_window',
                }

        if self.name:
            name_upper = self.name.upper()
            name_lower = self.name.lower()
            name_title = self.name.title()
            self.partner_domain(domain)
            if self.select_all:
                if self.name:
                    partner_ids = self.env['res.partner'].search(
                        [('name', 'ilike', self.name), '|', ('name', 'ilike', name_upper), '|', ('name', 'ilike', name_lower), ('name', 'ilike', name_title)]).ids
                    self.partner_view_return(partner_ids)
                partner_ids = self.env['res.partner'].search(['|', ('customer', '=', True), '|', ('supplier', '=', True), '|',
                                                              ('is_contact', '=', True), ('is_employee', '=', True)]).ids
                self.partner_view_return(partner_ids)

            if self.name:
                domain.append('|')
                domain.append(('name', 'ilike', self.name))
                domain.append('|')
                domain.append(('name', 'ilike', name_upper))
                domain.append('|')
                domain.append(('name', 'ilike', name_lower))
                domain.append(('name', 'ilike', name_title))

        partner_ids = self.env['res.partner'].search(domain).ids
        tree_view = self.env.ref('base.view_partner_tree')
        form_view = self.env.ref('base.view_partner_form')

        return {
            'name': _('Partners'),
            'view_mode': 'tree,form',
            'res_model': 'res.partner',
            'views': [
                (tree_view.id, 'tree'),
                (form_view.id, 'form')
            ],
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', partner_ids)],
        }
