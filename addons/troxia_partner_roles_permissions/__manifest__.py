# -*- coding: utf-8 -*-
# Copyright 2020 Troxia.
# Part of Troxia. See LICENSE file for full copyright and licensing details.
{
    "name": "Troxia Roles & Permissions",
    "description": """
       This module activate to assign roles and permission
       from the partner form.
    """,
    "category": 'Roles',
    'version': '14.0.0.1',
    'author': 'Troxia Enterprise Cloud Applications',
    'website': "https://troxia.cloud",
    'depends': ['base','portal', 'contacts'],
    'data': [
        'views/res_partner_view.xml',
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}
