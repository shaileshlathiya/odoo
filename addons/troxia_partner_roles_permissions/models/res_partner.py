# -*- coding: utf-8 -*-
# Copyright 2020 Troxia.
# Part of Troxia. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _
from odoo.exceptions import UserError


class ResPartner(models.Model):
    _inherit = 'res.partner'

    has_portal_access = fields.Boolean("Portal User")
    # has_freelancer_access = fields.Boolean("Freelancer")

    @api.onchange('has_portal_access')
    def check_email_set(self):
        if self.has_portal_access and not self.email:
            raise UserError(_("Please add email address first!"))

    @api.model
    def create(self, values):
        result = super(ResPartner, self).create(values)
        user_obj = self.env['res.users']
        related_user = user_obj.search([('partner_id', '=', result.id)], limit=1)
        group_ids = related_user.groups_id.ids or []
        if values.get('has_portal_access', False):
            self.create_portal_user(related_user, result, group_ids, values)
        return result

    def write(self, values):
        result = super(ResPartner, self).write(values)
        if not values.get('lang', False):
            user_obj = self.env['res.users']
            for partner in self:
                related_user = user_obj.search(['|', ('active', '=', False), ('active', '=', True),
                                                ('partner_id', '=', partner.id)], limit=1)
                group_ids = related_user.groups_id.ids or []
                if 'has_portal_access' in values and values.get('has_portal_access', False):
                    self.create_portal_user(related_user, self, group_ids, values)
        return result

    def create_portal_user(self, related_user, result, group_ids, values):
        if not related_user:
            related_user = self.env['res.users'].with_context(no_reset_password=True, created_from_partner=True).sudo().create({
                'email': result.email,
                'login': result.email,
                'partner_id': result.id,
                'company_id': 1,
                'company_ids': [(6, 0, [1])],
                'groups_id': [(6, 0, [])],
            })
        portal_group = self.env.ref('base.group_portal')
        if portal_group:
            group_ids.append(portal_group.id)
            related_user.groups_id = [(6, 0, group_ids)]
            # Send an email to portal user after activation of portal access.
            wizard_main = self.env['portal.wizard'].create({})
            wizard_user = self.env['portal.wizard.user'].create({
                'wizard_id': wizard_main.id,
                'in_portal': True,
                'partner_id': result.id,
                'user_id': related_user.id,
                'email': result.email})
            wizard_user.with_context(active_test=True)._send_email()
            wizard_user.refresh()

        elif 'has_portal_access' in values and not values.get('has_portal_access'):
            portal_group = self.env.ref('base.group_portal')
            if portal_group and portal_group.id in group_ids:
                group_ids.remove(portal_group.id)
                related_user.groups_id = [(6, 0, group_ids)]

