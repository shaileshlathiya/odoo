# -*- coding: utf-8 -*-
# Copyright 2020 Troxia.
# Part of Troxia. See LICENSE file for full copyright and licensing details.
from odoo import fields, models, api


class ResUsers(models.Model):
    _inherit = 'res.users'

    @api.model
    def create(self, values):
        result = super(ResUsers, self).create(values)
        if self._context.get('created_from_partner', False):
            return result
        portal_group = self.env.ref('base.group_portal')
        if portal_group.id in result.groups_id.ids:
            if not result.partner_id.has_portal_access:
                result.partner_id.has_portal_access = True
        return result

    def write(self, values):
        result = super(ResUsers, self).write(values)
        if not values.get('lang', False):
            if self._context.get('write_from_role', False):
                return result
            if self._context.get('param', False) and self._context.get('param').get('model') == 'res.partner':
                return result
            for user in self:
                portal_group = self.env.ref('base.group_portal')
                if portal_group.id in user.groups_id.ids:
                    if not user.partner_id.has_portal_access:
                        user.partner_id.has_portal_access = True
                else:
                    user.partner_id.has_portal_access = False
        return result
