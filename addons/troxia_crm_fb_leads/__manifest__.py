# -*- coding: utf-8 -*-
# Copyright 2020 Troxia.
# Part of Troxia. See LICENSE file for full copyright and licensing details.
{
    'name': 'Troxia CRM Facebook Leads',
    'category': 'CRM',
    'description':'Generate Leads from Facebook Ad Campaign',
    'summary':'Leads from Facebook Ad Campaign',
    'version': '14.0.0.1',
    'depends': [
        'crm',
        'troxia_crm'
    ],
    'data': [
        #Security Code
        'security/ir.model.access.csv',
        #Views
        'views/facebook_credentials.xml',
        'views/facebook_page.xml',
        'views/facebook_lead_form.xml',
        'views/default_fields_mapping.xml',
        'views/facebook_lead_form.xml',
        #CRON
        'data/ir_cron.xml',
        #Data
        'data/lead.form.default.mapping.csv',
    ],
    # Author
    'author': 'Troxia Enterprise Cloud Applications',
    'website': 'https://troxia.cloud',

    'installable': True,
    'application': False,
    'auto_install': False,
}
