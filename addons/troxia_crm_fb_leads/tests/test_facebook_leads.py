# -*- coding: utf-8 -*-
# Copyright 2020 Troxia.
# Part of Troxia. See LICENSE file for full copyright and licensing details.
from odoo.tests import common


class TestFacebookLeads(common.TransactionCase):

    def setUp(self):
        super(TestFacebookLeads, self).setUp()

    def test_create_data(self):
        self.fbCredentials = self.env['fb.credentials']
        self.fbPage = self.env['fb.page']
        self.leadForm = self.env['fb.lead.form']

        test_fb_credentials = self.fbCredentials.create({
            'name': "Test Credentials",
            'user_access_id': '1234567890',
            'user_access_token': 'abcdefgght12412xcwe3few',
        })

        test_fb_page = self.fbPage.create({
            'name': 'Test MMPage',
            'page_id': '9874650324',
            'page_access_token': 'asert7416e7rrt984d7h4e9rdf419ers7',
            # 'fb_credential_id': test_fb_credentials.id,
        })

        test_lead_form = self.leadForm.create({
            'name': 'Test Lead Form',
            'form_id': "38597416498",
        })

        # Check if the credential data match
        self.assertEqual(test_fb_credentials.name, "Test Credentials")
        self.assertEqual(test_fb_credentials.user_access_id, "1234567890")
        self.assertEqual(test_fb_credentials.user_access_token, "abcdefgght12412xcwe3few")

        # Change the user_access_id
        test_fb_credentials.write({
            'user_access_id': '19089078082022'
        })
        # Check if the user_access_id changed or not
        self.assertNotEqual(test_fb_credentials.user_access_id, "1234567890")


        # Check if the fb page data match
        self.assertEqual(test_fb_page.name, "Test MMPage")
        self.assertEqual(test_fb_page.page_id, "9874650324")
        self.assertEqual(test_fb_page.page_access_token, "asert7416e7rrt984d7h4e9rdf419ers7")

        # Change the user_access_id
        test_fb_page.write({
            'page_id': '1234567890'
        })
        # Check if the user_access_id changed or not
        self.assertNotEqual(test_fb_page.page_id, "9874650324")

