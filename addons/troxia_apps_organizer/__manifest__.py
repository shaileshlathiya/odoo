# -*- coding: utf-8 -*-
# Copyright 2020 Troxia.
# Part of Troxia. See LICENSE file for full copyright and licensing details.
{
    'name': "Troxia Apps Organizer",
    'summary': """
        Apps allows to Organize Troxia Apps.
        """,
    'description': """
        Apps allows to Organize Troxia Apps in different manner.
    """,
    'category': 'Tools',
    'website': "https://troxia.cloud",
    'author': 'Troxia Enterprise Cloud Applications',
    'version': '14.0.0.1',
    'depends': [
        'base',
        'base_setup'
    ],
    'data': [
        'views/assets.xml',
        'views/views.xml'
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}
