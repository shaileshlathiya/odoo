# -*- coding: utf-8 -*-
# Copyright 2020 Troxia.
# Part of Troxia. See LICENSE file for full copyright and licensing details.
from odoo import models, fields, api


class ResPartner(models.Model):
    _inherit = 'res.partner'

    # Roles and Permission fields
    ref_company = fields.Char("Ref. Company")

    association = fields.Boolean(string='Association', default=False)
    member = fields.Boolean(string='Member', default=False)
    insurance = fields.Boolean(string='Insurance', default=False)
    agency = fields.Boolean(string='Agency', default=False)
    agent = fields.Boolean(string='Agent', default=False)
    broker = fields.Boolean(string='Broker', default=False)
    producer = fields.Boolean(string='Producer', default=False)
    insured = fields.Boolean(string='Insured', default=False)
    attorney = fields.Boolean(string='Attorney', default=False)
    stakeholder = fields.Boolean(string='Stakeholder', default=False)
