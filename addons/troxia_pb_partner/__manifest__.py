# -*- coding: utf-8 -*-
# Copyright 2020 Troxia.
# Part of Troxia. See LICENSE file for full copyright and licensing details.
{
    "name": "Troxia - PB Partner Extend",
    'category': 'Partners',
    'version': '14.0.0.1',
    'summary': 'Troxia Partners Extemd for PB',
    'description': "Troxia Partners Extemd for PB",
    'depends': [
        'troxia_partner_roles_permissions',
        'troxia_partner_fields',
        'troxia_partners',
        'l10n_it_pec',
    ],
    'data': [
        'views/res_partner_view.xml',
    ],
    # Author
    'author': 'Troxia Enterprise Cloud Applications',
    'website': 'https://troxia.cloud',

    'installable': True,
    'application': False,
    'auto_install': False,
}
