# -*- coding: utf-8 -*-
# Copyright 2020 Troxia.
# Part of Troxia. See LICENSE file for full copyright and licensing details.
{
    # Module information
    'name': 'Troxia Partner Fields',
    'category': 'CRM',
    'version': '14.0.0.1',
    'summary': 'New custom fields',
    'description': "Added new fields in partner form ",
    'images': [],
    # Dependencies
    'depends': [
        'partner_firstname',
        'partner_contact_personal_information_page',
        'partner_contact_birthdate',
        'partner_contact_gender',
    ],
    # Views
    'data': [
        'views/res_partner_view.xml',
    ],
    # Author
    'author': 'Troxia Enterprise Cloud Applications',
    'website': 'https://troxia.cloud',

    # Technical
    'installable': True,
    'auto_install': False,
    'application': False,

}
