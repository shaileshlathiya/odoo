# -*- coding: utf-8 -*-
# Copyright 2020 Troxia.
# Part of Troxia. See LICENSE file for full copyright and licensing details.
from odoo import fields, models


class ResPartner(models.Model):

    _inherit = 'res.partner'

    phone2 = fields.Char("Phone 2", copy=False)
    mobile2 = fields.Char("Mobile 2", copy=False)
    fax = fields.Char("Fax", copy=False)
    source = fields.Char("Contact Source", translate=True)
    function = fields.Char(string='Role')

    troxia_res_street = fields.Char()
    troxia_res_street2 = fields.Char()
    troxia_res_zip = fields.Char(change_default=True)
    troxia_res_city = fields.Char()
    troxia_res_state_id = fields.Many2one(
        "res.country.state", string='State ', ondelete='restrict')
    troxia_res_country_id = fields.Many2one(
        'res.country', string='Country ', ondelete='restrict')
