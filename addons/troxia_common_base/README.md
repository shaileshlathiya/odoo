# Troxia Backend Common Base v1.0

- Troxia Enterprise Cloud Applications
- ir.attachment functionality
- DB Recognition

Credits
=======

Contributors
------------
- Troxia Enterprise Cloud Applications

Sponsors
--------
- Troxia Enterprise Cloud Applications

Maintainers
-----------
- Troxia Enterprise Cloud Applications

Further information
===================
contact :- https://troxia.cloud