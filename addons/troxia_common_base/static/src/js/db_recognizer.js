/* Copyright 2020 Troxia.
 * Part of Troxia. See LICENSE file for full copyright and licensing details.
 */
odoo.define('troxia_common_base.db_recognizer', function (require) {
    "use strict";

    var UserMenu = require('web.UserMenu');
    UserMenu.include({
         start: function () {
            var self = this;
            var session = this.getSession();
            return this._super.apply(this, arguments).then(function () {
                self.$('.oe_session_db').text(session.db);
            });
        },
    });
});
