# -*- coding: utf-8 -*-
# Copyright 2020 Troxia.
# Part of Troxia. See LICENSE file for full copyright and licensing details.
from odoo import models, fields, api, _
import math


class IrAttachment(models.Model):
    _inherit = 'ir.attachment'

    @api.depends('file_size')
    def _compute_size(self):
        for rec in self:
            if rec.file_size == 0:
                return "0B"
            size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
            i = int(math.floor(math.log(rec.file_size, 1024)))
            p = math.pow(1024, i)
            s = round(rec.file_size / p, 2)
            rec.size = "%s %s" % (s, size_name[i])

    @api.depends('mimetype')
    def _get_file_name(self):
        for rec in self:
            if rec.mimetype:
                file_type = rec.mimetype.split('/')
                rec.file_format = file_type[1].upper() if file_type[1] else ''

    size = fields.Char(string="File Size", compute='_compute_size', store=True)
    language_id = fields.Many2one('res.lang', string="Language", domain="[('active', '=', True)]")
    file_format = fields.Char(string="File Format", compute='_get_file_name', store=True)

