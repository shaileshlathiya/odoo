# -*- coding: utf-8 -*-
# Copyright 2020 Troxia.
# Part of Troxia. See LICENSE file for full copyright and licensing details.
{
    'name': "Troxia Backend Common",
    'description': """
        Module for common features between different tenants.
    """,
    'summary': """
        Common Backend features.
    """,
    'category': 'Backend',
    'website': "https://troxia.cloud",
    'author': 'Troxia Enterprise Cloud Applications',
    'version': '14.0.0.1',
    'depends': ['base'],
    'data':[
        #Backend
        'views/backend/ir_attachment.xml',
        #Template
        'views/assets.xml',
    ],
    'qweb': [
        'static/src/xml/db_recognizer.xml'
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}
