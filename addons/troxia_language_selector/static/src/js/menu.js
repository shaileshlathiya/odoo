/* Copyright 2020 Troxia.
 * Part of Troxia. See LICENSE file for full copyright and licensing details.
 */
odoo.define('troxia_language_selector.LangaugeSelection', function (require) {
    "use strict";

    const CustomJs = require('web.UserMenu');

    CustomJs.include({
        init: function () {
            this._super.apply(this, arguments);
            const self = this;
            const session = this.getSession();
            let lang_list = '';

            self._rpc({
                model: 'res.lang',
                method: 'search_read',
                domain: [],
                fields: ['name', 'code'],
                lazy: false,
            }).then(function (res) {
                _.each(res, function (lang) {
                    let a = '';
                    if (lang['code'] === session.user_context.lang) {
                        a = '<i class="fa fa-check"></i>';
                    } else {
                        a = '';
                    }
                    lang_list += '<a href="#" data-lang-menu="lang" data-lang-id="' + lang['code'] + '"><img class="flag" src="troxia_language_selector/static/src/img/flags/' + lang['code'] + '.png"/>' + ' ' + lang['name'] + ' ' + a + '</a><br/>';
                });
                lang_list += '<div role="separator" class="dropdown-divider"/>'
                self.$('switch-lang').replaceWith(lang_list);
            })
        },
        start: function () {
            const self = this;
            return this._super.apply(this, arguments).then(function () {
                self.$el.on('click', 'a[data-lang-menu]', function (ev) {
                    ev.preventDefault();
                    const f = self['_onMenuLang'];
                    f.call(self, $(this));
                });
            });
        },
        _onMenuLang: function (ev) {
            const self = this;
            const lang = ($(ev).data("lang-id"));
            const session = this.getSession();
            return this._rpc({
                model: 'res.users',
                method: 'write',
                args: [session.uid, {'lang': lang}],
            }).then(function (result) {
                self.do_action({
                    type: 'ir.actions.client',
                    res_model: 'res.users',
                    tag: 'reload_context',
                    target: 'current',
                });
            });
        },
    })

});
