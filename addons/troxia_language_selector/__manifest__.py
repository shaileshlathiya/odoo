# -*- coding: utf-8 -*-
# Copyright 2020 Troxia.
# Part of Troxia. See LICENSE file for full copyright and licensing details.
{
    'name': "Troxia Language Selector",

    'summary': """
        Change the language from user preference menu with only one click.""",

    'description': """
        Change the language from user preference menu with only one click.

        1. Adds Language Selection to the top right-hand User Menu.
        2. Adds Country flags  to the top right-hand User Menu.
    """,

    'category': 'Tools',
    'website': "https://troxia.cloud",
    'author': 'Troxia Enterprise Cloud Applications',
    'version': '14.0.0.1',
    'depends': [
        'base',
    ],
    'data': [
        'views/views.xml',
    ],
    'qweb': ['static/src/xml/base.xml'],

    'installable': True,
    'application': True,
}
