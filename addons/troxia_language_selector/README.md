# Troxia Language Selector

Troxia Language Selector is a tool that adds a Langugage Selection to the top right-hand User Menu.


# Features

  - Adds Language Selection to the top right-hand User Menu.
  - Adds Country flags to the top right-hand User Menu.
  - Provides 78 country flags.


> After installation of this module:
  If you want to Setup more flags, just rename the flag picture to local code of the country.
  You can find the pictures in "\troxia_language_selector\static\src\img\flags"



Credits
=======
----
Contributors
------------
- Troxia Enterprise Cloud Applications

Sponsors
--------
- Troxia Enterprise Cloud Applications

Maintainers
-----------
- Troxia Enterprise Cloud Applications

Further information
===================
Contact :- https://troxia.cloud
