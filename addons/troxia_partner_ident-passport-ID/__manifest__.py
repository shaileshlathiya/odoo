# -*- coding: utf-8 -*-
# Copyright 2020 Troxia.
# Part of Troxia. See LICENSE file for full copyright and licensing details.
{
    # Module information
    'name' : 'Troxia Partner Identification , Passport ID',
    'category' : 'Partner',
    'version' : '14.0.0.1',
    'summary': 'Partner Identification , Passport ID fields',
    'description': "Added new fields in partner form ",
    'images': [],
    # Dependencies
    'depends': [
        'base',
        'partner_contact_personal_information_page'
    ],
    # Views
    'data': [
       'data/data.xml',
	   'views/res_partner_view.xml',
    ],
    # Author
    'author': 'Troxia Enterprise Cloud Applications',
    'website': 'https://troxia.cloud',
    # Technical
    'installable': True,
    'auto_install': False,
    'application': False,
}
