/* Copyright 2020 troxia.
 * Part of Troxia. See LICENSE file for full copyright and licensing details.
 */

odoo.define("troxia_backend_theme.form_renderer", function (require) {
    "use strict";

    const config = require("web.config");
    const core = require("web.core");
    const FormRenderer = require("web.FormRenderer");

    // Responsive view "action" buttons
    FormRenderer.include({
        /**
         * In mobiles, put all statusbar buttons in a dropdown.
         *
         * @override
         */
        _renderHeaderButtons: function () {
            const $buttons = this._super.apply(this, arguments);
            if (
                !config.device.isMobile ||
                !$buttons.is(":has(>:not(.o_invisible_modifier))")
            ) {
                return $buttons;
            }
            // $buttons must be appended by JS because all events are bound
            $buttons.addClass("dropdown-menu");
            const $dropdown = $(
                core.qweb.render("troxia_backend_theme.MenuStatusbarButtons")
            );
            $buttons.addClass("dropdown-menu").appendTo($dropdown);
            return $dropdown;
        },
    });
});
