/* Copyright 2020 troxia.
 * Part of Troxia. See LICENSE file for full copyright and licensing details.
 */
odoo.define("troxia_backend_theme.field_upgrade", function(require) {
    "use strict";

    var FormRenderer = require("web.FormRenderer");

    FormRenderer.include({
        _renderTagForm: function(node) {
            var $result = this._super(node);

            if (this.state && this.state.model === "res.config.settings") {
                // Hide enterprise labels with related fields
                $result
                    .find(".o_enterprise_label")
                    .parent()
                    .parent()
                    .parent()
                    .hide();
            }
            return $result;
        },
    });
});
