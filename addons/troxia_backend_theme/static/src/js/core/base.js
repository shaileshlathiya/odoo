/* Copyright 2020 troxia.
 * Part of Troxia. See LICENSE file for full copyright and licensing details.
 */
odoo.define("troxia_backend_theme.base", function(require) {
    "use strict";
    var WebClient = require("web.WebClient");

    WebClient.include({
        init: function(parent) {
            this._super.apply(this, arguments);
            var self = this;
            this.set("title_part", {zopenerp: ""});
            odoo.debranding_new_name = "";
            odoo.debranding_new_website = "";
            odoo.debranding_new_title = "";

            self._rpc(
                {
                    model: "ir.config_parameter",
                    method: "get_debranding_parameters",
                },
                {
                    shadow: true,
                }
            ).then(function(result) {
                odoo.debranding_new_name = result["troxia_backend_theme.new_name"];
                odoo.debranding_new_website = result["troxia_backend_theme.new_website"];
                odoo.debranding_new_title = result["troxia_backend_theme.new_title"];
                self.set("title_part", {zopenerp: odoo.debranding_new_title});
            });
        },
    });
});
