/* Copyright 2020 troxia.
 * Part of Troxia. See LICENSE file for full copyright and licensing details.
 */
odoo.define("troxia_backend_theme.bot", function(require) {
    "use strict";

    require("troxia_backend_theme.dialog");
    var core = require("web.core");
    var Message = require("mail.model.Message");
    var session = require("web.session");
    // var MailBotService = require("mail_bot.MailBotService");
    var _t = core._t;

    Message.include({
        _getAuthorName: function() {
            if (this._isOdoobotAuthor()) {
                return "Bot";
            }
            return this._super.apply(this, arguments);
        },
        getAvatarSource: function() {
            if (this._isOdoobotAuthor()) {
                return "/web/binary/company_logo?company_id=" + session.company_id;
            }
            return this._super.apply(this, arguments);
        },
    });

    // MailBotService.include({
    //     getPreviews: function(filter) {
    //         var previews = this._super.apply(this, arguments);
    //         previews.map(function(preview) {
    //             if (preview.title === _t("OdooBot has a request")) {
    //                 preview.title = _t("TroxiaBot has a request");
    //             }
    //             if (preview.imageSRC === "/mail/static/src/img/odoobot.png") {
    //                 preview.imageSRC =
    //                     "/web/binary/company_logo?company_id=" + session.company_id;
    //             }
    //             return preview;
    //         });
    //         return previews;
    //     },
    // });
});
