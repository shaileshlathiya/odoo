/* Copyright 2020 troxia.
 * Part of Troxia. See LICENSE file for full copyright and licensing details.
 */

odoo.define("troxia_backend_theme.document_viewer", function (require) {
    "use strict";

    const core = require("web.core");
    const DocumentViewer = require("mail.DocumentViewer");

    // DocumentViewer: Add support to maximize/minimize
    DocumentViewer.include({
        // Widget 'keydown' and 'keyup' events are only dispatched when
        // this.$el is active, but now the modal have buttons that can obtain
        // the focus. For this reason we now listen core events, that are
        // dispatched every time.
        events: _.extend(
            _.omit(DocumentViewer.prototype.events, ["keydown", "keyup"]),
            {
                "click .o_maximize_btn": "_onClickMaximize",
                "click .o_minimize_btn": "_onClickMinimize",
                "shown.bs.modal": "_onShownModal",
            }
        ),

        start: function () {
            core.bus.on("keydown", this, this._onKeydown);
            core.bus.on("keyup", this, this._onKeyUp);
            return this._super.apply(this, arguments);
        },

        destroy: function () {
            core.bus.off("keydown", this, this._onKeydown);
            core.bus.off("keyup", this, this._onKeyUp);
            this._super.apply(this, arguments);
        },

        _onShownModal: function () {
            // Disable auto-focus to allow to use controls in edit mode.
            // This only affects the active modal.
            // More info: https://stackoverflow.com/a/14795256
            $(document).off("focusin.modal");
        },
        _onClickMaximize: function () {
            this.$el.removeClass("o_responsive_document_viewer");
        },
        _onClickMinimize: function () {
            this.$el.addClass("o_responsive_document_viewer");
        },
    });
});
