/* Copyright 2020 troxia.
 * Part of Troxia. See LICENSE file for full copyright and licensing details.
 */

odoo.define("troxia_backend_theme.apps_menu", function (require) {
    "use strict";

    const AppsMenu = require("web.AppsMenu");

    AppsMenu.include({
        events: _.extend(
            {
                "hide.bs.dropdown": "_hideAppsMenu",
            },
            AppsMenu.prototype.events
        ),

        /**
         * Add icon data directly to png format
         * @override
         */
        init: function (parent, menuData) {
            this._super.apply(this, arguments);
            for (const n in this._apps) {
                this._apps[n].web_icon_data = menuData.children[n].web_icon_data;
            }
        },

        /**
         * Prevent the AppSwitcher from being opened multi-times
         *
         * @override
         */
        _onAppsMenuItemClicked: function (ev) {
            this._super.apply(this, arguments);
            ev.preventDefault();
            ev.stopPropagation();
        },
        /*
         * Control if AppSwitcher can be closed
         */
        _hideAppsMenu: function () {
            return !this.$("input").is(":focus");
        },
    });
});
