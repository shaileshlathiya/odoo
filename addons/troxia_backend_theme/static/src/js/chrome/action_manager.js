/* Copyright 2020 troxia.
 * Part of Troxia. See LICENSE file for full copyright and licensing details.
 */

odoo.define("troxia_backend_theme.app_switcher", function (require) {
    "use strict";

    const ActionManager = require("web.ActionManager");
    const BasicController = require("web.BasicController");

     /* Hide AppSwitcher in desktop and mobile modes.
     * To avoid delays in pages with a lot of DOM nodes we make
     * sub-groups' with 'querySelector' to improve the performance.
     */
    function closeAppSwitcher() {
        _.defer(function () {
            // Need close AppSwitcher?
            var menu_apps_dropdown = document.querySelector(".o_menu_apps .dropdown");
            $(menu_apps_dropdown)
                .has(".dropdown-menu.show")
                .find("> a")
                .dropdown("toggle");
            // Need close Sections Menu?
            // TODO: Change to 'hide' in modern Bootstrap >4.1
            var menu_sections = document.querySelector(
                ".o_menu_sections li.show .dropdown-toggle"
            );
            $(menu_sections).dropdown("toggle");
            // Need close Mobile?
            var menu_sections_mobile = document.querySelector(".o_menu_sections.show");
            $(menu_sections_mobile).collapse("hide");
        });
    }

    BasicController.include({
        /**
         * Close the AppSwitcher if the data set is dirty and a discard dialog
         * is opened
         *
         * @override
         */
        canBeDiscarded: function (recordID) {
            if (this.model.isDirty(recordID || this.handle)) {
                closeAppSwitcher();
            }
            return this._super.apply(this, arguments);
        },
    });

    // Hide AppDrawer or Menu when the action has been completed
    ActionManager.include({
        /**
         * @override
         */
        _appendController: function() {
            this._super.apply(this, arguments);
            closeAppSwitcher();
        },
    });
});
