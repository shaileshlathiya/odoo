/* Copyright 2020 troxia.
 * Part of Troxia. See LICENSE file for full copyright and licensing details.
 */

odoo.define("troxia_backend_theme.sidebar", function (require) {
    "use strict";

    const config = require("web.config");
    const Menu = require("web.Menu");

    Menu.include({
        events: _.extend(
            {
                // Clicking a hamburger menu item should close the hamburger
                "click .o_menu_sections [role=menuitem]": "_onClickMenuItem",
                // Opening any dropdown in the navbar should hide the hamburger
                "show.bs.dropdown .o_menu_systray, .o_menu_apps": "_hideMobileSubmenus",
                //Show sidebar full menu
                 "click #sidebarToggle": "_showSidebarFullMenu",
                //Show sidebar icons only
                 "click #sidebarToggleClose": "_showSidebarIcons",
            },
            Menu.prototype.events
        ),

        start: function () {
            this.$menu_toggle = this.$(".o-menu-toggle");
            return this._super.apply(this, arguments);
        },

        /**
         * Hide menus for current app if you're in mobile
         */
        _hideMobileSubmenus: function () {
            if (
                config.device.isMobile &&
                this.$menu_toggle.is(":visible") &&
                this.$section_placeholder.is(":visible")
            ) {
                this.$section_placeholder.collapse("hide");
            }
        },
        /**
         * Show sidebar full menu
         */
        _showSidebarFullMenu: function () {
            $(".troxia-sidebar-panel").addClass('troxia-sidebar-panel-toggle');
            $(".troxia-sidebar-panel-toggle").removeClass('troxia-sidebar-panel');
            $("#sidebarToggleClose").show();
            $("#sidebarToggle").hide();
        },
        /**
         * Show sidebar icons only
         */
        _showSidebarIcons: function () {
            $(".troxia-sidebar-panel-toggle").addClass('troxia-sidebar-panel');
            $(".troxia-sidebar-panel").removeClass('troxia-sidebar-panel-toggle');
            $("#sidebarToggleClose").hide();
            $("#sidebarToggle").show();
        },
        /**
         *
         * Prevent hide the menu (should be closed when action is loaded)
         *
         * @param {ClickEvent} ev
         */
        _onClickMenuItem: function (ev) {
            ev.stopPropagation();
        },

        /**
         * No menu brand in mobiles
         *
         * @override
         */
        _updateMenuBrand: function () {
            if (!config.device.isMobile) {
                return this._super.apply(this, arguments);
            }
        },
    });

});
