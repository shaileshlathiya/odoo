# Troxia Backend Theme v1.0

- Troxia Enterprise Cloud Applications
- Sidebar
- AppSwitcher
- Web Responsive

Credits
=======

Contributors
------------
- Troxia Enterprise Cloud Applications

Sponsors
--------
- Troxia Enterprise Cloud Applications

Maintainers
-----------
- Troxia Enterprise Cloud Applications

Further information
===================
contact :- https://troxia.cloud