# -*- coding: utf-8 -*-
# Copyright 2020 Troxia.
# Part of Troxia. See LICENSE file for full copyright and licensing details.
from . import controllers
from . import models
from odoo import SUPERUSER_ID, api

MODULE = "_troxia_backend_theme"


def uninstall_hook(cr, registry):
    env = api.Environment(cr, SUPERUSER_ID, {})
    env["ir.model.data"]._module_data_uninstall([MODULE])


def post_load():

    from . import fields
