# -*- coding: utf-8 -*-
# Copyright 2020 Troxia.
# Part of Troxia. See LICENSE file for full copyright and licensing details.

from odoo.tests import common


class TestCaseCommonResUsers(common.TransactionCase):
    def test_cp_res_users(self):
        user_public = self.env.ref("base.public_user")

        self.assertEqual(user_public.chatter_position, "normal")
        user_public.with_user(user_public).write({"chatter_position": "sided"})
        self.assertEqual(user_public.chatter_position, "sided")
