# -*- coding: utf-8 -*-
# Copyright 2020 Troxia.
# Part of Troxia. See LICENSE file for full copyright and licensing details.
from odoo import api, models
from odoo.tools.translate import _

PARAMS = [
    ("troxia_backend_theme.new_name", _("Troxia Enterprise Cloud Applications")),
    ("troxia_backend_theme.new_title", _("Troxia Enterprise Cloud Applications")),
    ("troxia_backend_theme.new_website", "troxia.cloud"),
    ("troxia_backend_theme.new_documentation_website", "https://troxia.cloud"),
    ("troxia_backend_theme.favicon_url", ""),
    ("troxia_backend_theme.send_publisher_warranty_url", "0"),
    ("troxia_backend_theme.icon_url", ""),
    ("troxia_backend_theme.apple_touch_icon_url", ""),
]


def get_debranding_parameters_env(env):
    res = {}
    for param, default in PARAMS:
        value = env["ir.config_parameter"].sudo().get_param(param, default)
        res[param] = value.strip()
    return res


class IrConfigParameter(models.Model):
    _inherit = "ir.config_parameter"

    @api.model
    def get_debranding_parameters(self):
        return get_debranding_parameters_env(self.env)

    @api.model
    def create_debranding_parameters(self):
        for param, default in PARAMS:
            if not self.env["ir.config_parameter"].sudo().get_param(param):
                self.env["ir.config_parameter"].sudo().set_param(param, default or " ")
