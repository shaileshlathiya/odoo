# -*- coding: utf-8 -*-
# Copyright 2020 Troxia.
# Part of Troxia. See LICENSE file for full copyright and licensing details.
from odoo import api, models


class Channel(models.Model):
    _inherit = "mail.channel"

    @api.model
    def init_odoobot(self):
        channel = super(Channel, self).init_odoobot()
        if not channel:
            return
        channel.write({"name": "Troxia ERP Cloud"})
        return channel
