# -*- coding: utf-8 -*-
# Copyright 2020 Troxia.
# Part of Troxia. See LICENSE file for full copyright and licensing details.
from . import res_users
from . import ir_confing_param
from . import ir_actions
from . import ir_translations
from . import ir_ui_view
from . import mail_channel
from . import mail_message
from . import public_warranty
