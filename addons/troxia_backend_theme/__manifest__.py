# -*- coding: utf-8 -*-
# Copyright 2020 Troxia.
# Part of Troxia. See LICENSE file for full copyright and licensing details.
{
    'name': 'Troxia Backend Theme',
    'summary': """
        - Auto Adjustable Sidebar
        - AppSwitcher
        - Web Responsive
    """,
    'description': """
        - Auto Adjustable Sidebar
        - AppSwitcher
        - Web Responsive
    """,
    "category": "Themes/Backend",
    'website': "https://troxia.cloud",
    'author': 'Troxia Enterprise Cloud Applications',
    'version': '14.0.0.1',
    'depends': ['base', 'base_setup','web','mail'],
    'data': [
        #data
        'data/data.xml',
        #views
        'views/res_users.xml',
        'views/res_config_settings.xml',
        'views/ir_module_views.xml',
        #Security
        'security/ir.model.access.csv',
        'security/menu.xml',
        # Template
        'views/assets.xml',
        'views/web.xml',
        'views/sidebar.xml',
    ],
    'qweb': [
        "static/src/xml/*.xml",
    ],
    'installable': True,
    'application': True,
    'auto_install': True,
}